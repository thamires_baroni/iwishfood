//
//  MapaViewController.swift
//  iWishFood
//
//  Created by Usuário Convidado on 25/11/15.
//  Copyright © 2015 Usuário Convidado. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapaViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager = CLLocationManager()
    var session: NSURLSession?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.locationManager.requestWhenInUseAuthorization()
        //self.locationManager.requestAlwaysAuthorization()
        
        self.mapView.showsUserLocation = true
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            
        }
        
      
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        //print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.mapView.region = MKCoordinateRegionMakeWithDistance(locValue, 1200, 1200)
        
        
        let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        self.session = NSURLSession(configuration: sessionConfig)
        
        let url: NSURL = NSURL(string: "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + String(locationManager.location!.coordinate.latitude) + "," + String(locationManager.location!.coordinate.longitude) + "&rankby=distance&types=grocery_or_supermarket&key=AIzaSyAFlRT5xclTqcXtq64Zmtfl0b_02tbaIEo")!
        
        let task = self.session!.dataTaskWithURL(url, completionHandler: { (data, response, error) -> Void in
            if (error == nil) {
                //let json: String = String(data: data!, encoding: NSUTF8StringEncoding)!
                self.getPins(data!)
                
            } else {
                print("error")
            }
        })
        
        task.resume()
    }
    
    func getPins(data: NSData) -> String? {

        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data, options: [])
            
            var results:NSArray = json["results"] as! NSArray
            
            for result in results {
                
                let lat = result["geometry"]!!["location"]!!["lat"] as! Double
                let lon = result["geometry"]!!["location"]!!["lng"] as! Double
                
                let pin: PinAnnotation = PinAnnotation(coordinate: CLLocationCoordinate2DMake(lat, lon), title: result["name"] as! String, subtitle: "")
                
                self.mapView.addAnnotation(pin)
            }

            
        } catch {
            print("Erro no parser JSON")
            return nil
        }
        
        
        locationManager.stopUpdatingLocation()

        return nil
    }




}

