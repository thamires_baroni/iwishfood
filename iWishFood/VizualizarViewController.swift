//
//  VizualizarViewController.swift
//  iWishFood
//
//  Created by Usuário Convidado on 09/12/15.
//  Copyright © 2015 Usuário Convidado. All rights reserved.
//

import UIKit

class VizualizarViewController: UIViewController {
    
    var produto: ItemFood? = nil
    
    @IBOutlet weak var lblProduto: UILabel!
    @IBOutlet weak var imvFoto: UIImageView!
    @IBOutlet weak var lblPreco: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblProduto.text = produto?.nome
        lblPreco.text = String(Float((produto?.valor)!))
        imvFoto.image = UIImage(data: (produto?.foto)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
