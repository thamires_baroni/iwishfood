//
//  ViewController.swift
//  iWishFood
//
//  Created by Usuário Convidado on 25/11/15.
//  Copyright © 2015 Usuário Convidado. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,
    UITextFieldDelegate{

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var txtNome: UITextField!
    @IBOutlet weak var txtValor: UITextField!
   
    
    @IBAction func addItemFood(sender: AnyObject) {
        
        let formatter = NSNumberFormatter()
        formatter.numberStyle = NSNumberFormatterStyle.CurrencyPluralStyle
        //Adicionar task no banco
        // Cria uma variavel para referenciar a tabela task
        let entityDescription = NSEntityDescription.entityForName("ItemFood", inManagedObjectContext: self.managedObjectContext!)
        
        // Cria uma instancia da task
        let item = ItemFood(entity: entityDescription!, insertIntoManagedObjectContext: self.managedObjectContext)
        
        // Atribui o valor nome para instancia da task!
        item.nome = self.txtNome.text!
        print(item.nome)
        
        let aValor: String = self.txtValor.text!
        let newValor = aValor.stringByReplacingOccurrencesOfString(",", withString: ".")
        
        item.valor = NSNumber(float: (newValor as NSString).floatValue)
        
        print(item.valor)
        item.foto = UIImageJPEGRepresentation(self.imageView.image!, 1.0)
        //print(item.foto)

        // Salva a task criada!
        do {
            try self.managedObjectContext!.save()
            // Apos adicao com sucesso, retorna a tela de listagem de tasks!
            self.navigationController?.popViewControllerAnimated(true)! } catch {
                // Escreve erro quando ha!
                print("Erro ao salvar task")
        }
        
        
    }
    var managedObjectContext: NSManagedObjectContext?
    override func viewDidLoad() {
        super.viewDidLoad()
        txtValor.keyboardType = UIKeyboardType.NumbersAndPunctuation
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    // Trata alteracao de campo!
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (txtNome.resignFirstResponder()){
            txtValor.becomeFirstResponder()
        }else{
            txtValor.resignFirstResponder()
        }
        return true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func novaFoto(sender: AnyObject) {
        
        let pickerController:UIImagePickerController = UIImagePickerController()
        pickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        pickerController.delegate = self
        self.presentViewController(pickerController, animated: true, completion: nil)
        
        //http://www.appcoda.com/google-maps-api-tutorial/
        
        /*
        
        let actionSheet = UIAlertController (
            title: "Contato",
            message:"Escolha uma forma de contato",
            preferredStyle: .ActionSheet)
        
        actionSheet.addAction(UIAlertAction(
            title: "Cancelar",
            style: UIAlertActionStyle.Cancel,
            handler: nil))
        
        actionSheet.addAction(UIAlertAction(
            title: "Email",
            style: UIAlertActionStyle.Default,
            handler: { action in self.contactTypeLabel.text = "Email" }
            ))
        
        actionSheet.addAction(UIAlertAction(title: "Telefone",
            style: UIAlertActionStyle.Default,
            handler: { action in self.contactTypeLabel.text = "Telefone"}
            ))
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
        */
        
        
        /*
        let alert = UIAlertController (title: "Confirmação", message: "Deseja realmente gravar o contato?", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(
            title: "Não",
            style: UIAlertActionStyle.Cancel,
            handler: { _ in
                print("Alert - Não")
        }))
        
        alert.addAction(UIAlertAction(title: "Sim",
            style: UIAlertActionStyle.Default,
            handler: { _ in
                print("Alert - Sim")
                let sync = SyncServer()
                sync.delegate = self
                sync.sendInfo()
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
        */
        
        /*
        let actionSheet = UIAlertController (title: "Sucesso", message:"Informação gravada com sucesso!", preferredStyle: UIAlertControllerStyle.Alert)
        
        actionSheet.addAction(UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.Default,
            handler: nil))
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
        */
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let img:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imageView.image = img
        dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func fotoCamera(sender: AnyObject) {
        // Valida se o dispositivo possui camera
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            let imagePickerController:UIImagePickerController = UIImagePickerController()
            // Define o tipo do recurso a ser utilizado como camera
            imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera
            // Indica os tipos de media permitidos pela camera
            imagePickerController.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.Camera)!
            // Define seu delegate
            imagePickerController.delegate = self
            // Indica se permite ou nao edicao apos a foto
            imagePickerController.allowsEditing = false
            // Apresenta a viewcontroller da camera
            self.presentViewController(imagePickerController, animated: true, completion: nil);
            
        } else {
            let alert:UIAlertController = UIAlertController(title: "Erro!", message: "Dispositivo não possui câmera", preferredStyle: UIAlertControllerStyle.Alert);
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }

    }
}

