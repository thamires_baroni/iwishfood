//
//  ListaTableViewController.swift
//  iWishFood
//
//  Created by Nilson Filho on 25/11/15.
//  Copyright © 2015 Usuário Convidado. All rights reserved.
//

import UIKit
import CoreData

class ListaTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    
    var managedObjectContext: NSManagedObjectContext?
    var fetchedResultController: NSFetchedResultsController = NSFetchedResultsController()
    
    func setupCoreDataStack() {
        
        // Criação do modelo
        let modelURL:NSURL? =
        NSBundle.mainBundle().URLForResource("iWishFood", withExtension: "momd")
        
        let model = NSManagedObjectModel(contentsOfURL: modelURL!)
        
        // Criação do coordenador
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: model!)
        // Recupera o caminho da documents
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let docPath: NSURL = NSURL(fileURLWithPath: paths[0])
        print(docPath.absoluteString)
        
        // Cria o path com o nome do arquivo sqlite!
        let sqlitePath = docPath.URLByAppendingPathComponent("iWishFood.sqlite")
        
        // Associa o arquivo de persistencia ao coordinator, especificando o tipo SQLite e valida possiveis erros
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL:
                sqlitePath, options: nil) //metodo nao sobrepoem na memoria
        } catch {
            print("Erro ao associar o coordinator")
        }
        
        // Criação do contexto
        managedObjectContext = NSManagedObjectContext(concurrencyType:
            NSManagedObjectContextConcurrencyType.MainQueueConcurrencyType)//consulta usando MainThread
        
        managedObjectContext!.persistentStoreCoordinator = coordinator
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        if(segue.identifier == "segueVisualizar")
        {
            let visualizarVC: VizualizarViewController = segue.destinationViewController as! VizualizarViewController;
            let item = sender as! ItemFood
            visualizarVC.produto = item
            visualizarVC.title = "Visualização"

        }
        else if(segue.identifier == "editarSegue")
        {
            let editarVC: EditarViewController = segue.destinationViewController as! EditarViewController;
            let item = sender as! ItemFood
            editarVC.produto = item
            editarVC.title = "Edição"
            editarVC.managedProduto = item
        }
        else
        {
            let taskController:ViewController =
            segue.destinationViewController as! ViewController
            taskController.managedObjectContext = managedObjectContext
        }
    }
    
    func getFetchedResultController() {
        // Primeiro inicializamos um FetchRequest com dados da tabela Task
        let fetchRequest = NSFetchRequest(entityName: "ItemFood")
        
        // Definimos que o campo usado para ordenação será "nome”
        let sortDescriptor = NSSortDescriptor(key: "nome", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        //Iniciamos a propriedade fetchedResultController com uma instância de NSFetchedResultsController com o FetchRequest acima definido e sem opções de cache
        self.fetchedResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        // A controller será o delegate do fetch!
        self.fetchedResultController.delegate = self
        
        // Executa o Fetch!
        do {
            try self.fetchedResultController.performFetch()
        } catch {
            print("Erro ao executar o fetch")
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupCoreDataStack() // inicia metodo CoreDataTask
        self.getFetchedResultController()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let cont = self.fetchedResultController.fetchedObjects?.count {
            return cont
        }
        return 0
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ListaFoodTableView", forIndexPath:
            indexPath)
        let item: ItemFood = self.fetchedResultController.objectAtIndexPath(indexPath) as! ItemFood
        cell.textLabel?.text = item.nome!
        cell.detailTextLabel?.text = String(format:"R$ %0.2f", Float(item.valor!))
        cell.imageView?.image = UIImage(data: item.foto!)
        cell.accessoryType = UITableViewCellAccessoryType.DetailDisclosureButton
        return cell
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath)
    {
        
        
        let alert = UIAlertController (title: "Nova ação", message: "O que deseja fazer?", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        let item: ItemFood = self.fetchedResultController.objectAtIndexPath(indexPath) as! ItemFood
        alert.addAction(UIAlertAction(
            title: "Visualizar",
            style: UIAlertActionStyle.Default,
            handler: { action in self.performSegueWithIdentifier("segueVisualizar", sender: item) }
        ))
        
        /*alert.addAction(UIAlertAction(title: "Editar",
            style: UIAlertActionStyle.Default,
            handler: { action in self.performSegueWithIdentifier("editarSegue", sender: item) }
        ))*/
        
        /*alert.addAction(UIAlertAction(
            title: "Excluir",
            style: UIAlertActionStyle.Default,
            handler: { _ in
                print("Alert - Não")
        }))*/
        
        alert.addAction(UIAlertAction(
            title: "Cancelar",
            style: UIAlertActionStyle.Cancel,
            handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            // Delete the row from the data source
            let managedObject: NSManagedObject =
            self.fetchedResultController.objectAtIndexPath(indexPath) as! NSManagedObject
            self.managedObjectContext?.deleteObject(managedObject) // Salva a task criada!
            // Salva a task criada!
            do {
                try self.managedObjectContext?.save()
                
            } catch {
                // Escreve erro quando ha!
                print("Erro ao remover task")
            }
        } else
            if editingStyle == .Insert {
                // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
        
    }
    
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
