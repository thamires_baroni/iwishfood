//
//  PinAnnotation.swift
//  iWishFood
//
//  Created by Nilson Filho on 14/12/15.
//  Copyright © 2015 Usuário Convidado. All rights reserved.
//
import UIKit
import MapKit

class PinAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}
