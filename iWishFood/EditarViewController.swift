//
//  EditarViewController.swift
//  iWishFood
//
//  Created by Usuário Convidado on 09/12/15.
//  Copyright © 2015 Usuário Convidado. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class EditarViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,
UITextFieldDelegate {
    
    var produto: ItemFood? = nil
    var managedObjectContext: NSManagedObjectContext?
    var managedProduto:ItemFood?
    
    @IBOutlet weak var txtProduto: UITextField!
    @IBOutlet weak var txtPreco: UITextField!
    @IBOutlet weak var imgFoto: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtProduto.text = produto?.nome
        txtPreco.text = String(Float((produto?.valor)!))
        imgFoto.image = UIImage(data: (produto?.foto)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let img:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imgFoto.image = img
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (txtProduto.resignFirstResponder()){
            txtPreco.becomeFirstResponder()
        }else{
            txtPreco.resignFirstResponder()
        }
        return true
    }
    
    @IBAction func atualizar(sender: AnyObject) {
       
       managedProduto?.nome = txtProduto.text!
        let aValor: String = self.txtPreco.text!
        let newValor = aValor.stringByReplacingOccurrencesOfString(",", withString: ".")
        
        managedProduto?.valor = NSNumber(float: (newValor as NSString).floatValue)
       //managedProduto?.foto = UIImageJPEGRepresentation(self.imgFoto.image!, 1.0)
        
        /*do{
            try self.managedObjectContext!.save()
        } catch {
            print("Erro")
        }*/
        do {
            try self.managedObjectContext!.save()
            // Apos adicao com sucesso, retorna a tela de listagem de tasks!
            self.navigationController?.popViewControllerAnimated(true)! } catch {
                // Escreve erro quando ha!
                print("Erro ao salvar task")
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
