//
//  ItemFood+CoreDataProperties.swift
//  
//
//  Created by Usuário Convidado on 09/12/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ItemFood {

    @NSManaged var foto: NSData?
    @NSManaged var nome: String?
    @NSManaged var valor: NSNumber?

}
