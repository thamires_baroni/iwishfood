//
//  ItemFood+CoreDataProperties.swift
//  iWishFood
//
//  Created by Usuário Convidado on 25/11/15.
//  Copyright © 2015 Usuário Convidado. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ItemFood {

    @NSManaged var nome: String?
    @NSManaged var foto: NSData?
    @NSManaged var valor: NSDecimalNumber?

}
